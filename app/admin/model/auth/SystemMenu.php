<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/20
 * Time: 19:19
 */

namespace app\admin\model\auth;


use app\common\model\BaseModel;

/**
 * @property int $menu_id    
 * @property string $title 权限菜单名   
 * @property int $father_id 父级id   
 * @property string $path 前端路由路劲   
 * @property string $component 路由关联组件   
 * @property int $hidden 前端是否隐藏菜单   
 * @property int $sort 菜单排序  默认值 255
 * @property string $icon 关联图标   
 * @property string $name 路由组件名称   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 */
class SystemMenu extends BaseModel
{
    protected $pk = 'menu_id';

    public function api() {
        return $this->belongsToMany(Api::class,'menu_api','api_id','menu_id');
    }

    public function role() {
        return $this->belongsToMany(Role::class,'role_menu','role_id','menu_id');
    }

    public function children() {
        return $this->hasMany(SystemMenu::class,'father_id','menu_id')
            ->order('sort');
    }


    public static function getRouter($menus) {
        $condition = [];
        if($menus !== 'all') {
            $condition = ['menu_id'=>['in',$menus]];
        }
        return self::with(['children'=>function($query) use($condition) {
            $query->where($condition)->order('sort asc');
        }])->where('father_id',0)
            ->order('sort asc')
//            ->fetchSql()
            ->select();
    }

}