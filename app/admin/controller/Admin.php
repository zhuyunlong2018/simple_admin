<?php
/**
 * Created by bianquan
 * CommonUser: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/12
 * Time: 21:04
 */

namespace app\admin\controller;


use app\admin\annotation\Permission;
use app\admin\annotation\Validate;
use app\admin\validate\auth\AdminValidate;
use app\lib\exception\LoginException;
use app\lib\exception\ParameterException;
use app\lib\exception\ResourcesException;
use app\lib\Response;
use think\annotation\Inject;
use think\captcha\facade\Captcha;


/**
 * 管理员
 * Class Admin
 * @package app\admin\controller
 */
class Admin extends BaseController
{

    /**
     * @Inject()
     * @var \app\admin\logic\auth\Admin
     */
    protected $logic;

    /**
     * @Permission(desc="后台登录接口", type="OPEN", method="POST")
     * @return \think\response\Json
     * @throws ParameterException
     * @throws ResourcesException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws LoginException
     * @throws \app\lib\exception\AuthException
     */
    public function login()
    {
        return Response::result($this->logic->handleLogin());
    }

    /**
     * 验证码
     * @Permission(desc="获取登录验证码", type="OPEN", method="GET")
     * @return \think\Response
     */
    public function captcha() {
        return Captcha::create('verify');
    }

    /**
     * @Permission(desc="获取管理员列表", type="AUTH", method="GET")
     * @throws \think\db\exception\DbException
     */
    public function getList()
    {
        return Response::result($this->logic->getList());
    }

    /**
     * @Permission(desc="更新管理员", type="AUTH", method="PUT")
     * @Validate(AdminValidate::class, scene="update")
     * @throws ResourcesException
     */
    public function update()
    {
        return Response::result($this->logic->update());
    }

    /**
     * @Permission(desc="创建管理员", type="AUTH", method="POST")
     * @Validate(AdminValidate::class, scene="create")
     */
    public function create()
    {
        return Response::result($this->logic->create());
    }

    /**
     * @Permission(desc="删除管理员", type="AUTH", method="DELETE")
     * @Validate(AdminValidate::class, scene="delete")
     * @throws \app\lib\exception\DeleteException
     */
    public function delete()
    {
        $this->logic->delete();
        return Response::ok();
    }

    /**
     * @Permission(desc="修改密码接口", type="TOKEN", method="POST")
     * @return \think\response\Json
     * @throws ParameterException
     * @throws \app\lib\exception\AuthException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function changePassword() {
        return Response::result($this->logic->changePassword());
    }

    /**
     * @Permission(desc="刷新缓存", type="TOKEN", method="POST")
     * @return \think\response\Json
     * @throws \ReflectionException
     * @throws \app\lib\exception\DevException
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function refreshCache() {
        $this->logic->refreshCache();
        return Response::ok();
    }

    /**
     * @Permission(desc="退出登录接口", type="TOKEN", method="POST")
     */
    public function logout()
    {

    }

}