<?php
/**
 * Created by PhpStorm.
 * User: ZhuYunLong2018
 * Email: 920200256@qq.com
 * Date: 2020/2/22
 * Time: 11:14
 */

namespace app\lib;


trait Instance
{
    protected static $instance;

    protected function __construct()
    {

    }

    public static function instance(...$args)
    {
        if (self::$instance === null) {
            self::$instance = new static(...$args);
        }
        return self::$instance;
    }

}