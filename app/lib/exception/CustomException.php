<?php
namespace app\lib\exception;

use think\Exception;

/**
 * Class CustomException
 * 自定义异常类的基类
 */
class CustomException extends Exception
{
    public $code = 400;
    public $message = 'invalid parameters';
    public $errorCode = 999;
    public $data = [];

    public $shouldToClient = true;

    /**
     * 构造函数，接收一个关联数组
     * @param string $params 关联数组只应包含code、msg和errorCode，且不应该是空值
     * @param array $data
     */
    public function __construct($params = "", $data = [])
    {
        if (is_string($params) && !empty($params)) {
            $this->message = $params;
        }
        if(is_array($params)){
            if(array_key_exists('code',$params)){
                $this->code = $params['code'];
            }
            if(array_key_exists('msg',$params)){
                $this->message = $params['msg'];
            }
            if(array_key_exists('errorCode',$params)){
                $this->errorCode = $params['errorCode'];
            }
            if(array_key_exists('data',$params)){
                $this->data = $params['data'];
            }
        }
        if ($data) {
            $this->data = $data;
        }
    }
}

