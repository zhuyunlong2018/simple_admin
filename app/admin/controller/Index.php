<?php
/**
 * Created by bianquan
 * CommonUser: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/12
 * Time: 20:07
 */

namespace app\admin\controller;



use app\admin\annotation\Permission;
use think\facade\Log;

/**
 * Class Index
 * @package app\admin\controller
 */
class Index extends BaseController
{
    /**
     * @Permission(desc="测试接口", type="OPEN", method="GET")
     */
    public function index() {
        Log::info('app_action_run');
        echo 'index11';
    }

    public function test() {
        echo 'test';
    }
}