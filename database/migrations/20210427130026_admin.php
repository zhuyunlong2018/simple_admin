<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Admin extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('admin', [
            'engine' => 'InnoDB',
            'id' => 'admin_id',
            'comment' => '管理员'
        ]);
        $table->addColumn('password', 'string', [
            'limit' => 40,
            'default' => md5('123456'),
            'comment' => '管理员密码'
        ])
            ->addColumn('admin_name', 'string', ['limit' => 30, 'default' => '', 'comment' => '管理员账户'])
            ->addColumn('avatar', 'string', ['default' => '', 'comment' => '头像'])
            ->addColumn('mobile', 'string', ['limit' => 12, 'default' => '', 'comment' => '手机号码'])
            ->addColumn('email', 'string', ['limit' => 64, 'default' => '', 'comment' => '邮箱'])
            ->addColumn('status', 'boolean', ['limit' => 1, 'default' => 0, 'comment' => '状态'])
            ->addColumn('role_id', 'integer', ['limit' => 11, 'default' => 0, 'comment' => '绑定角色'])
            ->addColumn(Column::timestamp('create_time')->setDefault('CURRENT_TIMESTAMP')->setComment('创建时间'))
            ->addColumn(Column::timestamp('update_time')->setDefault('CURRENT_TIMESTAMP')->setComment('更新时间'))
            ->addColumn(Column::timestamp('delete_time')->setComment('删除时间')->setNullable())
            ->create();
    }

}
