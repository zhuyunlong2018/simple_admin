<?php


namespace app\lib\exception;

/**
 * token验证失败时抛出此异常 
 */
class TokenException extends CustomException
{
    public $code = 401;
    public $message = 'Token已过期或无效Token';
    public $errorCode = ErrorCode::TOKEN_EXCEPTION;
}