<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/21
 * Time: 16:59
 */

namespace app\common\model;


use think\Model;

class BaseModel extends Model
{

    protected $autoWriteTimestamp = 'timestamp';

}