<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/2/1
 * Time: 21:10
 */

namespace app\lib\exception;


class RepeatException extends CustomException
{
    public $code = 404;
    public $errorCode = ErrorCode::REPEAT_EXCEPTION;
    public $message = "resources repeat";
}