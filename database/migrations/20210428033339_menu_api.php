<?php

use think\migration\Migrator;
use think\migration\db\Column;

class MenuApi extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('menu_api', [
            'id' => 'menu_api_id',
            'comment' => '系统菜单关联api'
        ]);
        $table->addColumn('menu_id', 'integer', ['limit' => 6, 'default' => 0, 'comment' => '系统菜单id'])
            ->addColumn('api_id', 'integer', ['limit' => 6, 'default' => 0, 'comment' => 'api_id'])
            ->addColumn(Column::timestamp('create_time')->setDefault('CURRENT_TIMESTAMP')->setComment('创建时间'))
            ->addColumn(Column::timestamp('update_time')->setDefault('CURRENT_TIMESTAMP')->setComment('更新时间'))
            ->addColumn(Column::timestamp('delete_time')->setComment('删除时间')->setNullable())
            ->create();
    }
}
