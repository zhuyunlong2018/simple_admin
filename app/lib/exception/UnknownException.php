<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2019/2/22
 * Time: 9:10
 */

namespace app\lib\exception;


class UnknownException extends CustomException
{
    public $code = 501;
    public $message = '出现某种错误，休息一下';
    public $errorCode = ErrorCode::UN_KNOW_EXCEPTION;
}