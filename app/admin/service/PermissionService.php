<?php
declare (strict_types=1);

namespace app\admin\service;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;
use think\App;
use think\Service;


/**
 * 注册权限注解服务
 * Class PermissionService
 * @package app\admin\service
 */
class PermissionService extends Service
{
    use InteractsWithPermission;

    /** @var Reader */
    protected $reader;


    /**
     * 注册服务
     *
     * @return mixed
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function register()
    {
        $this->app->bind(Reader::class, new AnnotationReader());
    }

    /**
     * 执行服务
     *
     * @param Reader $reader
     * @return mixed
     */
    public function boot(Reader $reader)
    {
        $this->reader = $reader;
        //注解路由权限
        $this->registerAnnotationRoute();
    }
}
