<?php
// 应用公共文件



/**
 * 获取当前request参数数组,去除值为空
 * @return array
 */
function get_query(){
    $param=request()->except(['s']);
    $rst=array();
    foreach($param as $k=>$v){
        if(!empty($v)){
            $rst[$k]=$v;
        }
    }
    return $rst;
}
