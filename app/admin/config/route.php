<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/22
 * Time: 10:54
 */

return [
    'middleware' => [
        \think\middleware\SessionInit::class,
        \app\admin\middleware\CheckAuth::class,
    ]
];