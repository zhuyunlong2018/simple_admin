<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2019/3/1
 * Time: 10:38
 */

namespace app\lib\exception;


class DevException extends CustomException
{
    public $code = 505;
    public $message = '开发时异常';
    public $errorCode = ErrorCode::DEV_EXCEPTION;
}