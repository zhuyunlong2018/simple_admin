<?php


namespace app\lib\exception;

/**
 * token验证失败时抛出此异常 
 */
class RequestMethodException extends CustomException
{
    public $code = 401;
    public $message = '请求方式错误';
    public $errorCode = ErrorCode::REQUEST_METHOD_EXCEPTION;
}