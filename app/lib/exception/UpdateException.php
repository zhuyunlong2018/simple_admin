<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/16
 * Time: 16:21
 */

namespace app\lib\exception;

class UpdateException extends CustomException
{
    public $code = 513;
    public $message = '数据库更新错误';
    public $errorCode = ErrorCode::UPDATE_EXCEPTION;
}