<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/21
 * Time: 22:15
 */

namespace app\admin\controller;

use app\admin\annotation\Permission;
use app\admin\annotation\Validate;
use app\admin\validate\auth\MenuValidate;
use app\lib\Response;
use think\annotation\Inject;

class Menu extends BaseController
{

    /**
     * @Inject()
     * @var \app\admin\logic\auth\Menu
     */
    protected $logic;


    /**
     * @Permission(desc="获取系统菜单列表", type="AUTH", method="GET")
     */
    public function getList() {
        return Response::result($this->logic->getList());
    }

    /**
     * @Permission(desc="创建系统菜单", type="AUTH", method="POST")
     * @Validate(MenuValidate::class, scene="create")
     */
    public function create() {
        $this->logic->create();
        return Response::ok();
    }
    /**
     * @Permission(desc="修改系统菜单", type="AUTH", method="PUT")
     * @Validate(MenuValidate::class, scene="update")
     */
    public function update() {
        $this->logic->update();
        return Response::ok();
    }

    /**
     * @Permission(desc="删除系统菜单", type="AUTH", method="DELETE")
     * @Validate(MenuValidate::class, scene="delete")
     * @return \think\response\Json
     * @throws \app\lib\exception\DeleteException
     */
    public function delete() {
        $this->logic->delete();
        return Response::ok();
    }

    /**
     * @Permission(desc="获取用户权限菜单路由", type="TOKEN", method="GET")
     */
    public function getRouter() {
        return Response::result($this->logic->getRoleMenus());
    }

    /**
     * @Permission(desc="获取系统菜单和绑定的api", type="TOKEN", method="GET")
     */
    public function getMenu() {
        return Response::result($this->logic->getMenusWithApi());
    }

}