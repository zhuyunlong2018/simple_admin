<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/20
 * Time: 20:55
 */

namespace app\admin\model\auth;


use app\common\model\BaseModel;
use think\Cache;

/**
 * @property int $api_id    
 * @property string $api_name api描述   
 * @property string $api_key api key
 * @property string $method 请求方式   
 * @property string $type 权限类别   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 */
class Api extends BaseModel
{
    protected $pk = 'api_id';

    protected $autoWriteTimestamp = 'timestamp';


}