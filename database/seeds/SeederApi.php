<?php

use app\admin\cache\PermissionCache;
use app\admin\service\PermissionService;
use Doctrine\Common\Annotations\AnnotationReader;
use think\migration\Seeder;

class SeederApi extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     * @throws ReflectionException
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \app\lib\exception\DevException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function run()
    {
        app()->config->set(['controller_layer' => 'admin/controller'], 'route');
        $service = new PermissionService(app());
        $service->boot(new AnnotationReader());
        $service->scanControllerDirs();
        PermissionCache::instance()->saveDb();
    }
}