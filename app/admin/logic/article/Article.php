<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/11/1
 * Time: 18:23
 */

namespace app\admin\logic\article;


use app\admin\logic\RestLogic;
use think\annotation\Inject;

class Article extends RestLogic
{

    /**
     * @Inject()
     * @var \app\common\model\article\Article
     */
    protected $model;

    public function getList()
    {
        $query = $this->model->newQuery()
            ->with('category')
            ->field("id, title, category_id, status, content_short, create_time, update_time");
        return $this->handleGetList($query);
    }

    /**
     * @return array|null|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function detail()
    {
        $article = $this->model
            ->with('category')
            ->where('id', '=', input('id'))
            ->find();
        return $article;
    }

    /**
     * TODO 更新或添加操作的时候，表单赋值
     */
    protected function setForm()
    {
        $this->form = [
            'category_id' => input('category_id'),
            'title' => input('title'),
            'content_short' => input('content_short'),
            'content' => input('content'),
            'status' => input('status')
        ];
    }
}