<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/24
 * Time: 21:01
 */

namespace app\admin\cache;

use app\admin\enum\AuthEnum;
use app\admin\logic\auth\Role;
use app\lib\exception\AuthException;
use app\lib\Instance;
use think\facade\Log;


/**
 * 角色权限缓存
 * Class RoleAuthCache
 * @package app\admin\cache
 */
class RoleAuthCache extends CacheBase
{
    use Instance;

    /**
     * @var int
     */
    protected $roleID;

    protected function __construct($roleID)
    {
        parent::__construct();
        $this->roleID = $roleID;
    }

    protected function getCacheKey($roleID) {
        return 'simple_admin_role_auth_key:' . $roleID;
    }


    /**
     * 保存角色缓存
     * @param $roleID
     */
    public function saveCache($roleID) {
        $apiKeys = Role::instance()->getRoleApiKeys($roleID);
        $this->redis->del($this->getCacheKey($roleID));
        $this->redis->sAdd($this->getCacheKey($roleID), $apiKeys);
    }

    public function getCache() {
        return $this->redis->sMembers($this->getCacheKey($this->roleID));
    }

    /**
     * 判断角色是否有访问权限
     * @param $apiKey
     * @throws AuthException
     */
    public function check($apiKey) {
        if ($this->roleID === AuthEnum::SUPER_ROLE) {
            // 超级管理员默认
            return;
        }
        if (!$this->redis->sIsMember($this->getCacheKey($this->roleID), $apiKey)) {
            throw new AuthException('暂无权限访问该接口');
        }
    }
}