<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/16
 * Time: 16:21
 */

namespace app\lib\exception;




class InsertException extends CustomException
{
    public $code = 511;
    public $message = '数据库插入错误';
    public $errorCode = ErrorCode::INSERT_EXCEPTION;
}