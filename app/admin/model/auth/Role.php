<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/20
 * Time: 19:18
 */

namespace app\admin\model\auth;


use app\common\model\BaseModel;

/**
 * @property int $role_id    
 * @property string $role_name 角色名   
 * @property string $role_desc 角色描述   
 * @property string $menu_ids 绑定菜单ID   
 * @property string $half_menu_ids 存储子类没有全部拥有权限的父类menu_ids，用于适配前端el-tree组件   
 * @property string $api_ids 绑定api权限ID   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 */
class Role extends BaseModel
{
    protected $pk = 'role_id';

    public function getMenuIdsAttr($value) {
        if (empty($value)) {
            return [];
        }
        if (is_array($value)) {
            return $value;
        }
        return explode(',', $value);
    }

    public function getHalfMenuIdsAttr($value) {
        if (empty($value)) {
            return [];
        }
        if (is_array($value)) {
            return $value;
        }
        return explode(',', $value);
    }

    public function getApiIdsAttr($value) {
        if (empty($value)) {
            return [];
        }
        if (is_array($value)) {
            return $value;
        }
        return explode(',', $value);
    }
}