<?php

use app\admin\model\auth\Admin;
use think\migration\Seeder;

class SeederAdmin extends Seeder
{
    /**
     * 初始化超级管理员账户
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $admin = Admin::where('admin_name', 'admin')->find();
        if ($admin === null) {
            Admin::create([
                'admin_name' => 'admin',
                'status' => 1,
                'role_id' => 1
            ]);
        }
    }
}