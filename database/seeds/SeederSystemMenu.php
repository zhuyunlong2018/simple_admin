<?php

use app\admin\model\auth\Api;
use app\admin\model\auth\SystemMenu;
use think\migration\Seeder;

class SeederSystemMenu extends Seeder
{

    const MENU_DATA = [
        [
            'title' => '权限管理',
            'component' => 'layout/Layout',
            'path' => '/auth',
            'icon' => 'chart',
            'name' => 'authManager',
            'sub_page' => [
                [
                    'title' => '管理员管理',
                    'component' => 'auth/admin',
                    'path' => 'admin',
                    'icon' => '',
                    'name' => 'Admin',
                    'bind_api' => [
                        'Admin::getList',
                        'Admin::create',
                        'Admin::update',
                        'Admin::delete',
                        'Role::getList'
                    ]
                ],
                [
                    'title' => '角色管理',
                    'component' => 'auth/role',
                    'path' => 'role',
                    'icon' => '',
                    'name' => 'Role',
                    'bind_api' => [
                        'Role::getList',
                        'Role::create',
                        'Role::update',
                        'Role::delete',
                        'Menu::getList'
                    ]
                ],
                [
                    'title' => '菜单管理',
                    'component' => 'auth/menu',
                    'path' => 'menu',
                    'icon' => '',
                    'name' => 'Menu',
                    'bind_api' => [
                        'Menu::getList',
                        'Menu::create',
                        'Menu::update',
                        'Menu::delete',
                        'Api::getList'
                    ]
                ],
                [
                    'title' => '接口管理',
                    'component' => 'auth/api',
                    'path' => 'api',
                    'icon' => '',
                    'name' => 'Api',
                    'bind_api' => ['Api::getList']
                ]
            ]
        ],
        [
            'title' => '文章管理',
            'component' => 'layout/Layout',
            'path' => '/article',
            'icon' => 'chart',
            'name' => 'articleManager',
            'sub_page' => [
                [
                    'title' => '文章列表',
                    'component' => 'article/list',
                    'path' => 'list',
                    'icon' => '',
                    'name' => 'ArticleList',
                    'bind_api' => [
                        'Article::articles',
                        'Article::delete',
                    ]
                ],
                [
                    'title' => '文章分类',
                    'component' => 'article/category',
                    'path' => 'category',
                    'icon' => '',
                    'name' => 'Category',
                    'bind_api' => [
                        'Article::categories',
                        'Article::createCategory',
                        'Article::updateCategory',
                        'Article::deleteCategory',
                    ]
                ],
                [
                    'title' => '编辑文章',
                    'component' => 'article/detail',
                    'path' => 'detail',
                    'icon' => '',
                    'hidden' => 1,
                    'name' => 'ArticleDetail',
                    'bind_api' => [
                        'Article::detail',
                        'Article::categoryList',
                        'Article::update',
                        'Article::create',
                    ]
                ]
            ]
        ]
    ];

    /**
     * @var SystemMenu[]
     */
    protected $menus = [];

    /**
     * @var Api[]
     */
    protected $apis = [];

    /**
     * @param $mainMenu array
     */
    protected function createMainMenu($mainMenu)
    {
        $subMenus = $mainMenu['sub_page'];
        unset($mainMenu['sub_page']);
        $mainMenu['father_id'] = 0;
        /** @var SystemMenu $menu */
        $menu = SystemMenu::create($mainMenu);
        foreach ($subMenus as $subMenu) {
            $this->createSubMenu($menu, $subMenu);
        }
    }

    /**
     * @param $mainMenu SystemMenu
     * @param $subMenu array
     */
    protected function createSubMenu($mainMenu, $subMenu)
    {
        $apiList = $subMenu['bind_api'];
        unset($subMenu['bind_api']);
        $subMenu['father_id'] = $mainMenu->menu_id;
        $menu = SystemMenu::create($subMenu);
        foreach ($apiList as $api) {
            $this->createApiLink($menu, $api);
        }
    }

    /**
     * @param $menu SystemMenu
     * @param $api string
     */
    protected function createApiLink($menu, $api)
    {
        \app\admin\model\auth\MenuApi::create([
            'menu_id' => $menu->menu_id,
            'api_id' => $this->apis[$api]->api_id
        ]);
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        /** @var SystemMenu[] $menus */
        $menus = SystemMenu::select();
        foreach ($menus as $menu) {
            $this->menus[$menu->name] = $menu;
        }
        /** @var Api[] $apis */
        $apis = Api::select();
        foreach ($apis as $api) {
            $this->apis[$api->api_key] = $api;
        }
        foreach (self::MENU_DATA as $menuDatum) {
            if (isset($this->menus[$menuDatum['name']])) {
                // 主菜单不存在
                foreach ($menuDatum['sub_page'] as $subMenu) {
                    if (!isset($this->menus[$subMenu['name']])) {
                        // 子菜单不存在
                        $this->createSubMenu($this->menus[$menuDatum['name']], $subMenu);
                    }
                }
            } else {
                // 主菜单不存在，创建
                $this->createMainMenu($menuDatum);
            }
        }
    }
}