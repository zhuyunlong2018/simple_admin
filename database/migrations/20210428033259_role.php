<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Role extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('role', [
            'id' => 'role_id',
            'comment' => '角色表'
        ]);
        $table->addColumn('role_name', 'string', ['limit' => 30, 'default' => '', 'comment' => '角色名'])
            ->addColumn('role_desc', 'string', ['default' => '', 'comment' => '角色描述'])
            ->addColumn('menu_ids', 'string', ['limit' => 300, 'default' => '', 'comment' => '绑定菜单ID'])
            ->addColumn('half_menu_ids', 'string', ['limit' => 300, 'default' => '', 'comment' => '存储子类没有全部拥有权限的父类menu_ids，用于适配前端el-tree组件'])
            ->addColumn('api_ids', 'string', ['limit' => 400, 'default' => '', 'comment' => '绑定api权限ID'])
            ->addColumn(Column::timestamp('create_time')->setDefault('CURRENT_TIMESTAMP')->setComment('创建时间'))
            ->addColumn(Column::timestamp('update_time')->setDefault('CURRENT_TIMESTAMP')->setComment('更新时间'))
            ->addColumn(Column::timestamp('delete_time')->setComment('删除时间')->setNullable())
            ->create();

    }
}
