<?php

use think\migration\Migrator;
use think\migration\db\Column;

class Api extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('api', [
            'id' => 'api_id',
            'comment' => 'API表'
        ]);
        $table->addColumn('api_name', 'string', ['limit' => 255, 'default' => '', 'comment' => 'api描述'])
            ->addColumn(Column::string('api_key', 64)->setUnique()->setOptions(['default' => '', 'comment' => 'api关键key']))
            ->addColumn('method', 'string', ['limit' => 10, 'default' => '', 'comment' => '请求方式'])
            ->addColumn('type', 'string', ['limit' => 10, 'comment' => '权限类别'])
            ->addColumn(Column::timestamp('create_time')->setDefault('CURRENT_TIMESTAMP')->setComment('创建时间'))
            ->addColumn(Column::timestamp('update_time')->setDefault('CURRENT_TIMESTAMP')->setComment('更新时间'))
            ->addColumn(Column::timestamp('delete_time')->setComment('删除时间')->setNullable())
            ->create();

    }
}
