<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2020/2/8
 * Time: 21:34
 */

namespace app\lib\exception;


class ErrorCode
{

    //未登录错误
    const UN_LOGIN_EXCEPTION = 10000;

    //Token已过期或无效Token
    const TOKEN_EXCEPTION = 10001;

    //请求方式错误
    const REQUEST_METHOD_EXCEPTION = 10002;

    //登录失败
    const LOGIN_EXCEPTION = 10003;

    //权限不足
    const AUTH_EXCEPTION = 10004;

    //未绑定微信错误
    const UN_BIND_WX_EXCEPTION = 10005;

    //数据库删除错误
    const DELETE_EXCEPTION = 10010;

    //数据库插入错误
    const INSERT_EXCEPTION = 10011;

    //未检索到相关数据
    const SELECT_EXCEPTION = 10012;

    //数据库更新错误
    const UPDATE_EXCEPTION = 10013;

    //请求过于频繁
    const REQUEST_BUSILY_EXCEPTION = 10014;

    //传递参数错误
    const PARAMETER_EXCEPTION = 20000;

    //resources repeat
    const REPEAT_EXCEPTION = 20001;

    //resources not found
    const RESOURCE_EXCEPTION = 20002;

    //积分不足
    const POINTS_EXCEPTION = 20015;

    //出现某种错误，休息一下
    const UN_KNOW_EXCEPTION = 50001;

    //开发时异常
    const DEV_EXCEPTION = 60000;

    //JsSdk错误
    const WEIXIN_JS_SDK_EXCEPTION = 60001;




}