<?php

use think\migration\Migrator;
use think\migration\db\Column;

class ArticleCategory extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('article_category', [
            'id' => 'id',
            'comment' => '文章分类'
        ]);
        $table
            ->addColumn('name', 'string', ['limit' => 64, 'default' => '', 'comment' => '分类标题'])
            ->addColumn('status', 'boolean', ['limit' => 1, 'default' => 0, 'comment' => '是否展示，1展示，0隐藏'])
            ->addColumn(Column::timestamp('create_time')->setDefault('CURRENT_TIMESTAMP')->setComment('创建时间'))
            ->addColumn(Column::timestamp('update_time')->setDefault('CURRENT_TIMESTAMP')->setComment('更新时间'))
            ->addColumn(Column::timestamp('delete_time')->setComment('删除时间')->setNullable())
            ->create();
    }
}
