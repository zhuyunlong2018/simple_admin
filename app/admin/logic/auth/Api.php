<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2019/3/1
 * Time: 14:36
 */

namespace app\admin\logic\auth;

use app\admin\logic\RestLogic;
use think\annotation\Inject;

class Api extends RestLogic
{

    /**
     * @Inject()
     * @var \app\admin\model\auth\Api
     */
    protected $model;

    /**
     * 获取api列表
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList() {
        $type = $this->request->get('type', 'AUTH');
        $list = $this->model->where('type', $type)
            ->order('api_id', 'desc')
            ->select();
        return $list;
    }

    /**
     * 更新或添加操作的时候，表单赋值
     */
    protected function setForm()
    {
        // TODO: Implement setForm() method.
    }
}