<?php


namespace app\lib\exception;

/**
 * token验证失败时抛出此异常 
 */
class AuthException extends CustomException
{
    public $code = 401;
    public $message = '权限不足';
    public $errorCode = ErrorCode::AUTH_EXCEPTION;
}