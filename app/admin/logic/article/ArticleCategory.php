<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/11/1
 * Time: 18:23
 */

namespace app\admin\logic\article;


use app\admin\logic\RestLogic;
use think\annotation\Inject;

class ArticleCategory extends RestLogic
{

    /**
     * @Inject()
     * @var \app\common\model\article\ArticleCategory
     */
    protected $model;

    /**
     * 获取文章分类
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function categoryList() {
        $list = $this->model->newQuery()
            ->where('status', '=', 1)
            ->select();
        return $list;
    }


    /**
     * TODO 更新或添加操作的时候，表单赋值
     */
    protected function setForm()
    {
        $this->form = [
            'name' => input('name'),
            'status' => input('status')
        ];
    }
}