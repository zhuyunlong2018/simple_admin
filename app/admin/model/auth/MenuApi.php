<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/2/1
 * Time: 19:38
 */

namespace app\admin\model\auth;


use app\common\model\BaseModel;

/**
 * @property int $menu_api_id    
 * @property int $menu_id 系统菜单id   
 * @property int $api_id api_id   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 */
class MenuApi extends BaseModel
{

    protected $pk = 'menu_api_id';

}