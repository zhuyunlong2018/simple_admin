<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/7/12
 * Time: 20:57
 */

namespace app\lib\exception;


class WeixinJsSdkException extends CustomException
{
    public $code = 500;
    public $errorCode = ErrorCode::WEIXIN_JS_SDK_EXCEPTION;
    public $message = "JsSdk错误";
}