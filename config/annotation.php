<?php

return [
    'inject' => [
        'enable'     => true,
        'namespaces' => [],
    ],
    //注解路由关闭不用
    'route'  => [
        'enable'      => true,
        'controllers' => [],
    ],
    'model'  => [
        'enable' => true,
    ],
    'ignore' => [],
    'store'  => 'redis',//缓存store
];
