<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/21
 * Time: 21:54
 */

namespace app\admin\controller;

use app\admin\annotation\Permission;
use app\admin\annotation\Validate;
use app\admin\validate\auth\RoleValidate;
use app\lib\Response;
use think\annotation\Inject;

class Role extends BaseController
{

    /**
     * @Inject()
     * @var \app\admin\logic\auth\Role
     */
    protected $logic;

    /**
     * @Permission(desc="获取角色列表", type="AUTH", method="GET")
     */
    public function getList() {
        return Response::result($this->logic->getList());
    }

    /**
     * @Permission(desc="更新角色", type="AUTH", method="PUT")
     * @Validate(RoleValidate::class, scene="update")
     */
    public function update() {
        return Response::result($this->logic->update());
    }

    /**
     * @Permission(desc="创建角色", type="AUTH", method="POST")
     * @Validate(RoleValidate::class, scene="create")
     */
    public function create() {
        return Response::result($this->logic->create());
    }

    /**
     * @Permission(desc="删除角色", type="AUTH", method="DELETE")
     * @Validate(RoleValidate::class, scene="delete")
     */
    public function delete() {
        $this->logic->delete();
        return Response::ok();
    }

}