<?php

use think\migration\Migrator;
use think\migration\db\Column;

class SystemMenu extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('system_menu', [
            'id' => 'menu_id',
            'comment' => '系统菜单'
        ]);
        $table->addColumn('title', 'string', ['limit' => 255, 'default' => '', 'comment' => '权限菜单名'])
            ->addColumn('father_id', 'integer', ['limit' => 6, 'default' => 0, 'comment' => '父级id'])
            ->addColumn('path', 'string', ['limit' => 255, 'default' => '', 'comment' => '前端路由路劲'])
            ->addColumn('component', 'string', ['limit' => 255, 'default' => '', 'comment' => '路由关联组件'])
            ->addColumn('hidden', 'boolean', ['default' => false, 'comment' => '前端是否隐藏菜单'])
            ->addColumn('sort', 'integer', ['limit' => 3, 'default' => 255, 'comment' => '菜单排序'])
            ->addColumn(Column::string('icon', 255)->setNullable()
                ->setOptions(['default' => '', 'comment' => '关联图标']))
            ->addColumn(Column::string('name')->setUnique()->setOptions(['limit' => 255, 'default' => '', 'comment' => '路由组件名称']))
            ->addColumn(Column::timestamp('create_time')->setDefault('CURRENT_TIMESTAMP')->setComment('创建时间'))
            ->addColumn(Column::timestamp('update_time')->setDefault('CURRENT_TIMESTAMP')->setComment('更新时间'))
            ->addColumn(Column::timestamp('delete_time')->setComment('删除时间')->setNullable())
            ->create();
    }
}
