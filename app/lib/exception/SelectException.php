<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/20
 * Time: 10:14
 */

namespace app\lib\exception;



class SelectException extends CustomException
{
    public $code = 512;
    public $message = '未检索到相关数据';
    public $errorCode = ErrorCode::SELECT_EXCEPTION;
}