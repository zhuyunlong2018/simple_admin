<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/11/1
 * Time: 18:24
 */

namespace app\admin\controller;


use app\admin\annotation\Permission;
use app\admin\annotation\Validate;
use app\admin\validate\article\ArticleCategoryValidate;
use app\admin\validate\article\ArticleValidate;
use app\lib\Response;
use think\annotation\Inject;

class Article
{
    /**
     * @Inject()
     * @var \app\admin\logic\article\Article
     */
    protected $logic;

    /**
     * @Inject()
     * @var \app\admin\logic\article\ArticleCategory
     */
    protected $categoryLogic;

    /**
     * @Permission(desc="获取文章列表", type="AUTH", method="GET")
     * @return \think\response\Json
     */
    public function articles()
    {
        return Response::result($this->logic->getList());
    }

    /**
     * @Permission(desc="获取文章详情", type="AUTH", method="GET")
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function detail()
    {
        return Response::result($this->logic->detail());
    }

    /**
     * @Permission(desc="创建文章", type="AUTH", method="POST")
     * @Validate(ArticleValidate::class, scene="create")
     */
    public function create()
    {
        return Response::result($this->logic->create());
    }

    /**
     * @Permission(desc="编辑文章", type="AUTH", method="PUT")
     * @Validate(ArticleValidate::class, scene="update")
     * @return \think\response\Json
     * @throws \app\lib\exception\ResourcesException
     */
    public function update()
    {
        return Response::result($this->logic->update());
    }

    /**
     * @Permission(desc="删除文章", type="AUTH", method="DELETE")
     * @Validate(ArticleValidate::class, scene="delete")
     * @return \think\response\Json
     * @throws \app\lib\exception\DeleteException
     */
    public function delete()
    {
        $this->logic->delete();
        return Response::ok();
    }

    /**
     * @Permission(desc="获取文章开放的分类列表", type="AUTH", method="GET")
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function categoryList()
    {
        return Response::result($this->categoryLogic->categoryList());
    }

    /**
     * @Permission(desc="获取文章分类列表", type="AUTH", method="GET")
     * @return \think\response\Json
     */
    public function categories()
    {
        return Response::result($this->categoryLogic->getList());
    }

    /**
     * @Permission(desc="创建文章分类", type="AUTH", method="POST")
     * @Validate(ArticleCategoryValidate::class, scene="create")
     * @return \think\response\Json
     */
    public function createCategory()
    {
        return Response::result($this->categoryLogic->create());
    }

    /**
     * @Permission(desc="编辑文章分类", type="AUTH", method="PUT")
     * @Validate(ArticleCategoryValidate::class, scene="update")
     * @return \think\response\Json
     * @throws \app\lib\exception\ResourcesException
     */
    public function updateCategory()
    {
        return Response::result($this->categoryLogic->update());
    }

    /**
     * @Permission(desc="删除文章分类", type="AUTH", method="DELETE")
     * @Validate(ArticleCategoryValidate::class, scene="delete")
     * @return \think\response\Json
     * @throws \app\lib\exception\DeleteException
     */
    public function deleteCategory()
    {
        return Response::result($this->categoryLogic->delete());
    }
}