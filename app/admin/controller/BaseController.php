<?php
/**
 * Created by bianquan
 * CommonUser: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/12
 * Time: 20:07
 */

namespace app\admin\controller;


use app\admin\logic\RestLogic;

class BaseController extends \app\BaseController
{

    /**
     * @var RestLogic
     */
    protected $logic;
}