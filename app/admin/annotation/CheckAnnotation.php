<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/24
 * Time: 20:18
 */

namespace app\admin\annotation;


interface CheckAnnotation
{
    /**
     * 校验注解类使用是否符合规范
     * @param $class string 类名称
     * @return mixed
     */
    function check(string $class);
}