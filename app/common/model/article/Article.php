<?php
declare (strict_types = 1);

namespace app\common\model\article;

use app\common\model\BaseModel;

/**
 * 文章列表
 * create_time: 2021-11-01 18:22:08
 * @property int $id    
 * @property int $category_id 分类ID   
 * @property string $title 文章标题   
 * @property string $content_short 文章简介
 * @property string $content 文章内容
 * @property int $status 是否展示，1展示，0隐藏
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 * @mixin \think\Model
 */
class Article extends BaseModel
{

    public function category() {
        return $this->hasOne(ArticleCategory::class, 'id', 'category_id');
    }

    public function getStatusAttr($value) {
        return $value === 1;
    }

    public function setStatus($value) {
        return $value ? 1 : 0;
    }



}
