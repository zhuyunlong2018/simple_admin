<?php

use app\admin\model\auth\Role;
use think\migration\Seeder;

class SeederRole extends Seeder
{
    /**
     * 初始化超级管理员角色
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $role = Role::find(1);
        if ($role === null) {
            Role::create([
                'role_name' => 'admin',
                'role_desc' => '超级管理员角色',
                'api_ids' => '',
            ]);
        }

    }
}