<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2020/8/11
 * Time: 14:58
 */

namespace app\lib;


use think\facade\Cache;

class Redis
{
    use Instance;


    /**
     * @var \Redis
     */
    protected $redis;

    private function __construct($dbIndex = 0)
    {
        $this->init($dbIndex);
    }

    /**
     * 获取redis实例
     * @param int $dbIndex
     * @return object
     */
    public function init($dbIndex = 0)
    {
        if (is_null($this->redis)) {
            $this->redis = Cache::store('redis')->handler();
        }
        $this->redis->select($dbIndex);
        return $this->redis;
    }

    /*
     *KEYS => 接收数组，删除一个或多个缓存
     */
    public function del($key)
    {
        return $this->redis->del($key);

    }

    /*
     * KEYS => 设定键的存活时间
     */
    public function expire($key, $time)
    {
        return $this->redis->expire($key, $time);
    }

    /*
     * KEYS => 判断键是否存在
     */
    public function exists($key)
    {
        return $this->redis->exists($key);
    }


    /*
     * STRING => 获取键值对
     */
    public function get($key)
    {
        // 是否一次取多个值
        $func = is_array($key) ? 'mGet' : 'get';
        return $this->redis->{$func}($key);
    }


    /*
     * STRING => 设置键值对
     */
    public function set($key, $value, $expire = 0)
    {
        // 永不超时
        if ($expire == 0) {
            $ret = $this->redis->set($key, $value);
        } else {
            $ret = $this->redis->setex($key, $expire, $value);
        }
        return $ret;
    }


    /*
     * STRING => 递增
     */
    public function incr($key, $value = 1)
    {
        if ($value > 1) {
            return $this->redis->incrBy($key, $value);
        }
        return $this->redis->incr($key);
    }
    /*
     * STRING => 递减
     */
    public function decr($key, $value = 1)
    {
        if ($value > 1) {
            return $this->redis->decr($key, $value);
        }
        return $this->redis->decr($key);
    }

    /*
     *HASH => 获取一个多hash的一个或多个字段
     */
    public function hGet($key, $field)
    {
        $func = is_array($field) ? 'hmGet' : 'hGet';
        return $this->redis->{$func}($key, $field);
    }

    /*
     *HASH => 设置一个hash字段或字段数组
     */
    public function hSet($key, $field, $value = null)
    {
        if (is_array($field)) {
            return $this->redis->hmSet($key, $field);
        }
        return $this->redis->hSet($key, $field, $value);
    }

    /**
     * HASH => 移除某个或多个字段
     * @param $key
     * @param mixed ...$fields
     * @return bool|int
     */
    public function hDel($key, ...$fields) {
        return $this->redis->hDel($key, ...$fields);
    }

    /*
     *HASH => 获取一个哈希的所有字段值
     */
    public function hGetAll($key)
    {
        return $this->redis->hGetAll($key);
    }

    /*
     * SET => 判断元素是否在集合中
     */
    public function sIsMember($key, $field)
    {
        return $this->redis->sIsMember($key, $field);
    }

    /*
     * SET => 集合中添加元素
     */
    public function sAdd($key, $value)
    {
        if (is_array($value)) {
            return $this->redis->sadd($key, ...$value);
        } else {
            return $this->redis->sadd($key, $value);
        }
    }

    /*
     * SET => 提取集合中所有元素
     */
    public function sMembers($key)
    {
        return $this->redis->sMembers($key);
    }

    /**
     * 设置bit位
     * @param string $key
     * @param int $offset
     * @param int $value
     * @return int
     */
    public function setBit(string $key, int $offset, int $value = 1) {
        return $this->redis->setBit($key, $offset, $value);
    }

    /**
     * 获取bit位
     * @param string $key
     * @param int $offset
     * @return int
     */
    public function getBit(string $key, int $offset) {
        return $this->redis->getBit($key, $offset);
    }

    /**
     * 计算bit数
     * @param string $key
     * @return int
     */
    public function bitCount(string $key) {
        return $this->redis->bitCount($key);
    }
}