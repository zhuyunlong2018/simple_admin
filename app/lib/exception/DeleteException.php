<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/16
 * Time: 16:21
 */

namespace app\lib\exception;


class DeleteException extends CustomException
{
    public $code = 510;
    public $message = '数据库删除错误';
    public $errorCode = ErrorCode::DELETE_EXCEPTION;
}