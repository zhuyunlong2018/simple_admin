<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2020/9/4
 * Time: 10:26
 */

namespace app\lib\exception;


class RequestBusilyException extends CustomException
{
    public $code = 401;
    public $message = '请求过于频繁';
    public $errorCode = ErrorCode::REQUEST_BUSILY_EXCEPTION;
}