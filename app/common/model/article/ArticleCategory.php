<?php
declare (strict_types = 1);

namespace app\common\model\article;

use app\common\model\BaseModel;

/**
 * 文章分类
 * create_time: 2021-11-01 18:22:12
 * @property int $id    
 * @property string $name 分类标题   
 * @property int $status 是否展示，1展示，0隐藏   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 * @mixin \think\Model
 */
class ArticleCategory extends BaseModel
{
    

    public function getStatusAttr($value) {
        return $value === 1;
    }

    public function setStatus($value) {
        return $value ? 1 : 0;
    }


}
