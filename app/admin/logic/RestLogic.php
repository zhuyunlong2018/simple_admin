<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/22
 * Time: 18:08
 */

namespace app\admin\logic;


use app\common\model\BaseModel;
use app\lib\exception\DeleteException;
use app\lib\exception\ResourcesException;
use app\lib\Instance;
use think\db\BaseQuery;
use think\db\exception\DbException;
use think\db\Query;
use think\facade\Log;
use think\facade\Request;

abstract class RestLogic
{
    use Instance;

    /**
     * TODO 每个logic都继承rest都需要绑定对应的模型
     * @var BaseModel
     */
    protected $model;

    /**
     * @var Request
     */
    protected $request;

    /**
     * 是否导出excel
     * @var bool
     */
    protected $exportExcel = false;

    /**
     * form提交的数据
     * @var array
     */
    protected $form = [];

    /**
     * 表单提交处理后是否直接返回前端
     * @var bool
     */
    protected $formReturn = true;


    /**
     * table的检索条件
     * @var array
     */
    protected $query = [];

    protected $isUpdate = false;

    protected $isCreate = false;

    protected $isDelete = false;


    public function __construct()
    {
        $this->request = Request::instance();
        if ($this->request->get('fetch_table_data')) {

            //ajax获取table数据时，组装query条件
            $query = $this->request->get('query');
            if (!empty($query)) {
                try {
                    $this->query = json_decode($query, true);
                } catch (\Exception $exception) {
                    //请求参数解析错误
                }
            }
        }
        if ($this->request->get('export_data')) {
            //需要导出excel
            $this->exportExcel = true;
        }
    }


    /**
     * 获取table搜索关键词内容
     * @param $key
     * @param bool $default
     * @return bool|string
     */
    protected function getQuery($key, $default = false)
    {
        if (isset($this->query[$key])) {
            $query = trim($this->query[$key]);
            if ($query !== "") {
                return $query;
            }
        }
        return $default;
    }

    /**
     * 获取table分页数据
     * @param BaseQuery $query
     * @param bool $paginate 是否分页
     * @return \think\Collection
     * @throws DbException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function handleGetList(BaseQuery $query, $paginate = true)
    {

        $query->order(
            $this->getQuery('sort', 'create_time'),
            $this->getQuery('order', 'desc')
        );
        if ($paginate && $this->exportExcel === false) {
            return $query->paginate(
                array_merge($this->query, ['list_rows' => $this->getQuery('limit', 10)]),
                false
            );
        }
        return $query->select();
    }

    /**
     * rest 获取分页列表
     */
    public function getList() {
        $query = $this->model->newQuery();
        return $this->handleGetList($query);
    }

    /**
     * rest 添加一条数据
     */
    public function create()
    {
        $this->isCreate = true;
        $this->setForm();
        return $this->model->create($this->form)->toArray();
    }

    /**
     * rest 更新一条数据
     */
    public function update()
    {
        $this->isUpdate = true;
        $key = $this->model->getPk();
        $check = $this->model->find($this->request->put($key));
        if ($check === null) {
            throw new ResourcesException('id不正确');
        }
        $this->setForm();
        $result = $this->model->update($this->form, [
            $key => $this->request->put($key)
        ]);
        return $result->toArray();
    }

    /**
     * rest 移除一条数据
     */
    public function delete()
    {
        $this->isDelete = true;
        $result = $this->model->where(
            $this->model->getPk(),
            '=',
            $this->request->delete($this->model->getPk())
        )
            ->delete();
        if (!$result) {
            throw new DeleteException('删除失败');
        }
    }

    /**
     * TODO 更新或添加操作的时候，表单赋值
     */
    abstract protected function setForm();

}