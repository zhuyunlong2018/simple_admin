
#### 代码生成命令行，覆盖原tp make:model方法

生成模型：
```shell
php think make:model admin@test/Admin
```

注意：模型名称需要严格按照大驼峰规范
创建模型之前，需要数据库中已有该表

以上命令将自动生成如下文件，系统将自动从模型对应数据表中取出字段添加到模型属性注解中，

如果文件已存在，则不会创建，而是更新注释。

```php

<?php
declare (strict_types = 1);

namespace app\admin\model\test;

use app\common\model\BaseModel;

/**
 * 
 * create_time: 2021-05-05 15:34:20
 * @property int $admin_id    
 * @property string $password 管理员密码  默认值 e10adc3949ba59abbe56e057f20f883e
 * @property string $admin_name 管理员账户   
 * @property string $avatar 头像   
 * @property string $mobile 手机号码   
 * @property string $email 邮箱   
 * @property int $status 状态   
 * @property int $role_id 绑定角色   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 * @mixin \think\Model
 */
class Admin extends BaseModel
{
    protected $pk = 'admin_id';



}

```