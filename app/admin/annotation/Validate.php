<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/24
 * Time: 18:44
 */

namespace app\admin\annotation;


use app\lib\exception\DevException;
use Doctrine\Common\Annotations\Annotation;

/**
 * 注解的校验器
 * Class Validate
 * @package think\annotation\route
 * @Annotation
 * @Annotation\Target({"METHOD"})
 */
class Validate extends Annotation implements CheckAnnotation
{
    /**
     * @var string
     */
    public $scene;


    /**
     * 校验注解类使用是否符合规范
     * @param $class string 类名称
     * @return mixed
     * @throws DevException
     */
    function check(string $class)
    {
        if (!class_exists($this->value)) {
            throw new DevException($class . '类不存在');
        }
        if (empty($this->scene)) {
            throw new DevException($class . '校验scene未填写');
        }
    }
}