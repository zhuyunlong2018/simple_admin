<?php
declare (strict_types = 1);

namespace app\admin\listener;

use app\admin\cache\PermissionCache;
use think\facade\Log;

class ApiInjection
{
    /**
     * 事件监听处理
     *
     * @param $event
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function handle($event)
    {
        //将api注入mysql表中存储
        PermissionCache::instance()->saveDb();
    }
}
