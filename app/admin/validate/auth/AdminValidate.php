<?php
declare (strict_types=1);

namespace app\admin\validate\auth;

use app\admin\enum\AuthEnum;
use app\admin\model\auth\Admin;
use app\common\validate\BaseValidate;


class AdminValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'admin_id' => 'require|number|notSuper',
        'admin_name' => 'require|nameRepeat|max:32',
        'password' => 'require|max:32'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'admin_name.require' => '名称为必填项',
        'admin_name.max' => '名称最多不能超过32个字符',
    ];

    protected $scene = [
        'create' => [
            'admin_name',
            'password',
            'role_id'
        ],

        'update' => [
            'admin_id',
            'admin_name',
            'role_id',
        ],
        'delete' => [
            'admin_id'
        ]
    ];

    /**
     * 管理员用户名不能重复
     * @param $value
     * @param string $rule
     * @param string $data
     * @param string $field
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function nameRepeat($value, $rule = '', $data = '', $field = '')
    {
        if (empty($value)) {
            return $field . '不允许为空';
        }
        $check = Admin::when(isset($data['admin_id']), function ($query) use ($data) {
            $query->where('admin_id', '<>', $data['admin_id']);
        })
            ->where('admin_name', '=', $value)
            ->find();
        if ($check !== null) {
            return $field . '账户已存在';
        }
        return true;
    }

    /**
     * 管理员账户不能被操作
     * @param $value
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function notSuper($value)
    {
        $adminID = $this->request->delete('admin_id');
        if ($adminID == AuthEnum::SUPER_ADMIN) {
            return '超级管理员不能操作';
        }
        $admin = Admin::where('admin_id', '=', $value)
            ->find();
        if (empty($admin)) {
            return '未找到对应账户';
        }
        return true;
    }
}
