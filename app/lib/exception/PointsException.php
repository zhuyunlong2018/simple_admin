<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/21
 * Time: 16:10
 */

namespace app\lib\exception;


class PointsException extends CustomException
{
    public $code = 411;
    public $errorCode = ErrorCode::POINTS_EXCEPTION;
    public $message = "积分不足";
}