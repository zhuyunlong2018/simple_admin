<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/11/2
 * Time: 13:59
 */

namespace app\admin\controller;


use app\admin\annotation\Permission;
use app\lib\exception\ResourcesException;
use app\lib\exception\UnknownException;
use app\lib\Response;
use think\facade\Filesystem;
use think\facade\Request;

class Upload
{
    /**
     * @Permission(desc="上传图片", type="TOKEN", method="POST")
     * @return \think\response\Json
     * @throws UnknownException
     */
    public function uploadFile()
    {
        $file = request()->file('image');
        try {
            validate(['imgFile' => [
                'fileExt' => 'xls,xlsx,doc,docx,jpg,jpeg,png,bmp,gif,mov,mp4,wav,mp3,ogg,acc,webm'
            ]])
                ->check(['imgFile' => $file]);
            $saveName = Filesystem::disk('public')
                ->putFile('images', $file, 'date');
            $fileName = Request::domain() . '/storage/' . $saveName;
            return Response::result($fileName);
        } catch (\Exception $exception) {
            throw new UnknownException($exception->getMessage());
        }
    }

}