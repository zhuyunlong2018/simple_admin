<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'make:model' => \app\admin\command\make\Model::class,
        'app-init' => \app\command\AppInitialization::class,
    ],
];
