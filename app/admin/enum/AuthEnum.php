<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/25
 * Time: 11:22
 */

namespace app\admin\enum;


use PhpEnum\Enum;


/**
 * 角色权限相关枚举
 * Class RoleAuth
 * @package app\admin\enum
 */
class AuthEnum extends Enum
{
    /**
     * 超级管理员角色ID
     */
    const SUPER_ROLE = 1;

    /**
     * 超级管理员ID
     */
    const SUPER_ADMIN = 1;
}