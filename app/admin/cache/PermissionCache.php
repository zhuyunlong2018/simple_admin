<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/22
 * Time: 14:35
 */

namespace app\admin\cache;


use app\admin\annotation\Permission;
use app\admin\model\auth\Api;
use app\lib\Instance;
use think\facade\Db;
use think\facade\Log;

/**
 * 系统路由权限注解缓存
 * Class PermissionCache
 * @package app\admin\cache
 */
class PermissionCache extends CacheBase
{
    use Instance;

    protected function __construct()
    {
        parent::__construct();
    }

    const CACHE_KEY = 'simple_admin_permission_key';

    /**
     * 是否需要更新数据库信息
     * @var bool
     */
    protected $shouldUpdate = false;

    /**
     * 从缓存获取所有列表
     * @return Permission[]
     */
    public function getList()
    {
        $list = $this->redis->hGetAll(self::CACHE_KEY);
        if (is_array($list)) {
            foreach ($list as $key => $item) {
                $list[$key] = unserialize($item);
            }
        }
        return $list;
    }

    /**
     * 保存缓存
     * @param array $list
     */
    public function saveList(array $list)
    {
        foreach ($list as $key => $item) {
            $list[$key] = serialize($item);
        }
        $this->shouldUpdate = true;
        $this->redis->del(self::CACHE_KEY);
        $this->redis->hSet(self::CACHE_KEY, $list);
    }

    /**
     * 获取一个
     * @param string $field
     * @return Permission
     */
    public function getOne(string $field)
    {
        $permission = $this->redis->hGet(self::CACHE_KEY, $field);
        if ($permission) {
            return unserialize($permission);
        }
        return null;
    }

    /**
     * 添加一个
     * @param string $field
     * @param Permission $permission
     */
    public function addOne(string $field, Permission $permission)
    {
        $this->shouldUpdate = true;
        $this->redis->hSet(self::CACHE_KEY, $field, serialize($permission));
    }

    /**
     * 将api数据保存到mysql
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function saveDb()
    {
        if (!$this->shouldUpdate) {
            return;
        }
        $now = date('Y-m-d H:i:s');
        $list = $this->getList();
        $dbList = (new Api)->select();
        $keys = [];
        $insertData = [];
        $updateData = [];
        foreach ($list as $key => $item) {
            $keys[] = $item->key;
            /** @var Api $dbCache */
            $dbCache = $dbList->where('api_key', $item->key)->pop();
            if ($dbCache === null) {
                //不存在，新增的操作
                $insertData[] = [
                    'api_name' => $item->desc,
                    'api_key' => $item->key,
                    'method' => $item->method,
                    'type' => $item->type,
                    'create_time' => $now,
                    'update_time' => $now
                ];
            } else {
                // 更新操作, 如果修改了才进行更新
                if (
                    $dbCache->api_name !== $item->desc ||
                    $dbCache->type !== $item->type ||
                    $dbCache->method !== $item->method
                ) {
                    $updateData[] = [
                        'api_id' => $dbCache->api_id,
                        'api_name' => $item->desc,
                        'method' => $item->method,
                        'type' => $item->type,
                        'update_time' => $now
                    ];
                }
            }
        }
        Db::startTrans();
        try {
            //移除已删除的api方法
            if ($dbList->count() > count($keys)) {
                $deleteIDs = [];
                $dbList->each(function ($item) use (&$deleteIDs, $keys) {
//                    Log::info($item->api_key);
                    if (!in_array($item->api_key, $keys)) {
                        $deleteIDs[] = $item->api_id;
                    }
                });
                if (count($deleteIDs) > 0) {
                    Api::destroy($deleteIDs);
                }
            }

            //更新操作
            if (count($updateData) > 0) {
                (new Api())->saveAll($updateData);
            }
            //插入操作
            if (count($insertData) > 0) {
                (new Api())->insertAll($insertData);
            }
            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Log::error($e);
            Db::rollback();
        }
    }

}