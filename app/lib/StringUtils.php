<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/9/13
 * Time: 14:56
 */

namespace app\lib;


class StringUtils
{
    use Instance;

    public function generateRandString($length) {
        //字符组合
        $str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $len = strlen($str)-1;
        $randStr = '';
        for ($i=0;$i<$length;$i++) {
            $num=mt_rand(0,$len);
            $randStr .= $str[$num];
        }
        return $randStr;
    }
}