<?php
declare (strict_types=1);

namespace app\admin\validate\auth;

use app\admin\enum\AuthEnum;
use app\admin\model\auth\Role;
use app\common\validate\BaseValidate;


class RoleValidate extends BaseValidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'role_id' => 'require|number|notSuper',
        'role_name' => 'require|nameRepeat|max:32',
        'role_desc' => 'require|max:64',
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'role_name.require' => '名称为必填项',
        'role_name.max' => '名称最多不能超过32个字符',
    ];

    protected $scene = [
        'create' => [
            'role_name',
            'role_desc'
        ],

        'update' => [
            'role_id',
            'role_name',
            'role_desc'
        ],
        'delete' => [
            'role_id'
        ]
    ];

    /**
     * 角色用户名不能重复
     * @param $value
     * @param string $rule
     * @param string $data
     * @param string $field
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function nameRepeat($value, $rule = '', $data = '', $field = '')
    {
        if (empty($value)) {
            return $field . '不允许为空';
        }
        $check = Role::when(isset($data['role_id']), function ($query) use ($data) {
            $query->where('role_id', '<>', $data['role_id']);
        })
            ->where('role_name', '=', $value)
            ->find();
        if ($check !== null) {
            return $field . '角色已存在';
        }
        return true;
    }

    /**
     * 超级管理员角色不能被操作
     * @param $value
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    protected function notSuper($value)
    {
        $roleID = $this->request->delete('role_id');
        if ($roleID == AuthEnum::SUPER_ROLE) {
            return '超级管理员不能操作';
        }
        $role = Role::where('role_id', '=', $value)
            ->find();
        if (empty($role)) {
            return '未找到对应角色';
        }
        return true;
    }
}
