<?php
/**
 * Created by PhpStorm
 * User: ZhuYunlong2018
 * Email: 920200256@qq.com
 * Date: 2021/4/24
 * Time: 21:01
 */

namespace app\admin\cache;


use app\lib\Redis;

class CacheBase
{

    /**
     * @var Redis;
     */
    protected $redis;

    protected function __construct()
    {
        $this->redis = Redis::instance();
    }

}