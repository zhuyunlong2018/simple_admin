<?php
namespace app\lib;


/**
 * Class Response
 * @package app\lib 统一成功返回类
 */
class Response
{

    public static function result($data, $msg = 'success')
    {
        return json([
            'code' => 200,
            'data' => $data,
            'msg' => $msg
        ]);
    }

    public static function ok($msg = 'message')
    {
        return json([
            'code' => 200,
            'data' => [],
            'msg' => $msg
        ]);
    }
}

