<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2019/2/22
 * Time: 9:10
 */

namespace app\lib\exception;


class UnBindWxException extends CustomException
{
    public $code = 301;
    public $message = '未绑定微信错误';
    public $errorCode = ErrorCode::UN_BIND_WX_EXCEPTION;
}