<?php
/**
 * Created by bianquan
 * CommonUser: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/13
 * Time: 10:09
 */

namespace app\admin\model\auth;


use app\common\model\BaseModel;


/**
 * @property int $admin_id    
 * @property string $password 管理员密码  默认值 e10adc3949ba59abbe56e057f20f883e
 * @property string $admin_name 管理员账户   
 * @property string $avatar 头像   
 * @property string $mobile 手机号码   
 * @property string $email 邮箱   
 * @property int $status 状态   
 * @property int $role_id 绑定角色   
 * @property string $create_time 创建时间  默认值 CURRENT_TIMESTAMP
 * @property string $update_time 更新时间  默认值 CURRENT_TIMESTAMP
 * @property string $delete_time 删除时间   
 */
class Admin extends BaseModel
{

    protected $pk = 'admin_id';

    protected $hidden = ['password'];



}