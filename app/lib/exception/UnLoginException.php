<?php
/**
 * Created by bianquan
 * CommonUser: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/13
 * Time: 10:24
 */

namespace app\lib\exception;


class UnLoginException extends CustomException
{
    public $code = 401;
    public $message = '未登录错误';
    public $errorCode = ErrorCode::UN_LOGIN_EXCEPTION;
}