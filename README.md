simple_admin
===============

#### 一个功能非常简单的管理后台

#### 主要特性：

基于thinkPHP6.0和VUE的前后端分类admin管理后台

- 注解权限，自动注入API和权限管理
- 注解验证器

推荐使用PHPStorm，建议安装PHP Annotations插件：https://plugins.jetbrains.com/plugin/7320-php-annotations ，可以支持注解的自动完成。

部署需要环境：

- php: 7.1+
- mysql: 5.7
- redis
- composer


[前端simple_admin_vue地址](https://gitee.com/zhuyunlong2018/simple_admin_vue.git "前端地址")





```shell

# 下载源码

$ git clone https://gitee.com/zhuyunlong2018/simple_admin.git

 # 进入项目目录
$ cd simple_admin

# 安装项目依赖
$ composer install

# 初始化配置文件
$ php think app-init
# 后台admin入口为ADMIN_ENTRY的随机码，例如 http://localhost:8000/29hfw9fhw9
# 在.env中修改本地mysql redis配置

# 根据.env的mysql配置，创建对应的数据库

 # 生成数据表
$ php think migrate:run

 # 数据表注入初始化数据
$ php think seed:run

 # 运行项目
$ php think run

# 访问项目：http://localhost:8000/
    
```


### 开发说明：
- APP_DEBUG = true 模式下，每次请求都将更新控制器注解类，生产环境中需要设置APP_DEBUG = false，缓存注解配置以减少性能开销
- 请注意，APP_DEBUG = false模式下，文件修改注解后，可能存在缓存而无法立即生效问题，开发时修改代码，请打开APP_DEBUG

- 开发一个restful风格的模块，假设为blog模块的文章表，你需要以下四个文件


``` shell
# 创建模型类，请务必使用以下命令生成，框架已覆盖tp6自带的make:model，生成的模型将带有表字段属性注释，方便开发
$ php think make:model admin@blog/Article

# 创建校验类，请注意命名规范，创建好后，你需要添加create、update、delete三个接口scene
$ php think make:validate admin@blog/ArticleValidate

# 创建逻辑类


# 创建控制器，控制器接口请参考规范所述
$ php think make:controller admin@/blog/Article

```

#### Nginx Rewrite
```text
location / {
	if (!-e $request_filename){
		rewrite  ^(.*)$  /index.php?s=$1  last;   break;
	}
}

#目录禁止执行PHP文件
location ~* /(storage|static)/(.*).(php)$ {
    return 403;
}
```

- 请注意，框架采用多应用模式，如果在controller下继续创建目录分层，则访问路由需要在类上添加@Group注解，接口上添加@Route注解，否认无法实现路由注入

#### 规范

- 保持控制器尽量简洁，所有控制器方法只做请求分发，所有逻辑类全部放入logic命名空间内
 新建控制器后，建议在类中自动注入相关的逻辑类，控制器创建建议参考如下形式

    `@Inject`注解将自动在该控制器实例化时注入相应的logic类

    `@Permission`注解将声明对应接口方法的描述、权限类别和访问方式，并注入mysql供管理员权限配置和redis缓存中进行该api的权限校验
    
    `@Validate`注解将声明该接口访问时的校验类，并在访问时激活声明的校验场景进行校验



```php
namespace app\admin\controller;

use app\admin\annotation\Permission;
use app\admin\annotation\Validate;
use app\admin\validate\auth\AdminValidate;
use app\lib\Response;
use think\annotation\Inject;

/**
 * 管理员
 * Class Admin
 * @package app\admin\controller
 */
class Admin extends BaseController
{

	/**
     * @Inject()
     * @var \app\admin\logic\auth\Admin
     */
    protected $logic;

    /**
     * @Permission(desc="更新管理员", type="AUTH", method="PUT")
     * @Validate(AdminValidate::class, scene="update")
     */
    public function update()
    {
        return Response::result($this->logic->update());
    }
}

    
```


- 使用restful风格开发接口，所有的获取数据都使用GET操作，更新数据PUT，创建数据POST，删除数据DELETE

- 逻辑处理，代码已经封装好了restful风格的逻辑处理，只要是处理rest接口的，都可以直接继承RestLogic基类进行简化操作，如下直接创建一个admin逻辑类，只要继承RestLogic基类和注入模型类后，实现setForm方法，就满足基本的增删改查功能了。
```php
namespace app\admin\logic\auth;

use app\admin\logic\RestLogic;
use think\annotation\Inject;


class Admin extends RestLogic
{

	/**
     * @Inject()
     * @var \app\admin\model\auth\Admin
     */
    protected $model;

	/**
     * 获取编辑表单
     */
    protected function setForm()
    {
        $this->form = [
            'admin_name' => input('admin_name'),
            'role_id' => input('role_id')
        ];
        $password = trim(input('password'));
        if (!empty($password)) {
            $this->form['password'] = md5($password);
        }
    }

}

```
