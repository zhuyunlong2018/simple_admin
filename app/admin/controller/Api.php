<?php
/**
 * Created by bianquan
 * User: ZhuYunlong
 * Email: 920200256@qq.com
 * Date: 2019/1/23
 * Time: 19:55
 */

namespace app\admin\controller;


use app\admin\annotation\Permission;
use app\admin\cache\RoleAuthCache;
use app\lib\Response;
use think\annotation\Inject;

class Api extends BaseController
{

    /**
     * @Inject()
     * @var \app\admin\logic\auth\Api
     */
    protected $logic;

    /**
     * @Permission(desc="获取API列表", type="AUTH", method="GET")
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList()
    {
        return Response::result($this->logic->getList());
    }

    /**
     * @Permission(desc="获取用户api权限列表", type="TOKEN", method="GET")
     * @return \think\response\Json
     */
    public function getAuthList() {
        return Response::result(RoleAuthCache::instance()->getCache());
    }

}